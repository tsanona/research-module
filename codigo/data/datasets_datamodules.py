"""Module for dataset creation."""

import gc
import logging
import math
from contextlib import contextmanager
from copy import deepcopy
from pathlib import Path
from typing import Callable, Sequence, cast

import lightning.pytorch as pl
import numpy as np
import omegaconf
import torch
import torchvision
import zarr
from einops import rearrange
from lightning.pytorch.trainer.trainer import RunningStage, TrainerFn
from sklearn.model_selection import KFold, train_test_split
from torch.utils.data import DataLoader, Dataset, WeightedRandomSampler
from torchvision.transforms.functional import center_crop, crop, pad

from codigo.configs import Config
from codigo.data.utils import DatasetItemT, _custom_collate_fn
from codigo.paths import DATA_SOURCES, RUNS_LOG_DIR
from codigo.utils import partition

logger = logging.getLogger(__name__)


class SceneDataset(Dataset):
    """Dataset composed scenes and respective masks.

    Args:
        scene_names: list of names from scenes to be included.
        config: Config class instance.
        stage: Running mode of dataset: train, val, test or predict
    """

    def __init__(self, scene_names: Sequence[str], config: Config, stage: RunningStage):
        self.scene_names = scene_names
        self.config = config
        self.stage = stage
        if (transforms := config.dataset.data_transforms) is not None:
            transforms_list = [
                transform.instantiate() for transform in transforms.values()
            ]
            self.transforms = torchvision.transforms.Compose(transforms_list)
        else:
            self.transforms = None
        self.root_group = zarr.open_consolidated(
            zarr.LMDBStore(
                str(DATA_SOURCES / f"{config.dataset.dataset_name}" / "scenes.lmdb"),
                readonly=True,
                lock=False,
            ),
            mode="r",
        )

    def __len__(self):
        return len(self.scene_names)

    def __del__(self):
        self.root_group.store.close()

    @staticmethod
    def _reshape_and_pad_data(
        data: torch.Tensor, label: torch.Tensor, padding_needed: np.ndarray
    ):
        # data -> (X, h, w), stacked_data -> (X+1, h, w)
        data = data.reshape((-1, *data.shape[-2:]))
        stacked_data = torch.vstack([data, label.unsqueeze(0)])

        # randomizes amount of padding in each side
        top_left = np.random.randint(padding_needed + 1)
        # top_left = padding_needed // 2
        bottom_right = padding_needed - top_left

        # (X+1, >=crop_size, >=crop_size)
        return pad(
            stacked_data, padding=[*np.flip(top_left), *np.flip(bottom_right)], fill=-1
        )

    @staticmethod
    def _split_prepared_data(
        prepared_data: torch.Tensor, dim: int
    ) -> tuple[torch.Tensor, torch.Tensor]:
        data, labels = torch.tensor_split(prepared_data, [-1], dim=dim)
        return data, torch.clip(labels.squeeze(dim=dim), 0, None)

    def _crop_data(
        self,
        data: torch.Tensor,
        label: torch.Tensor,
        crop_size: int,
        random: bool,
    ) -> tuple[torch.Tensor, torch.Tensor]:
        # data -> ((t), c, h, w), label -> (h, w)
        original_data_shape = data.shape
        image_shape = np.array(original_data_shape[-2:])
        match self.config.dataset.padding_type:
            case "tight":
                padding_needed = np.clip(crop_size - image_shape, 0, None)

                # (X+1, >=crop_size, >=crop_size)
                prepared_data = self._reshape_and_pad_data(data, label, padding_needed)

            case "patch_size":
                # pad to the smallest multiple of crop_size that contains image_shape.
                patches_per_dim = np.ceil(image_shape / crop_size).astype("int")
                padding_needed = patches_per_dim * crop_size - image_shape
                # (((t) * c) + 1, crop_size * a, crop_size * b) where a, b = patches_per_dim
                prepared_data = self._reshape_and_pad_data(data, label, padding_needed)
            case _:
                raise NotImplementedError

        image_shape = np.array(prepared_data.shape[-2:])

        if np.any(image_shape > crop_size):
            if random:
                crop_params = [
                    *np.random.randint(np.clip(image_shape - crop_size, 0, None) + 1),
                    crop_size,
                    crop_size,
                ]
                prepared_data = crop(prepared_data, *crop_params)
            else:
                prepared_data = center_crop(prepared_data, [crop_size])

        data, label = self._split_prepared_data(prepared_data, dim=0)

        return data.reshape((*original_data_shape[:-2], crop_size, crop_size)), label

    def _batch_complete_image(
        self, data: torch.Tensor, label: torch.Tensor, crop_size: int
    ) -> tuple[torch.Tensor, torch.Tensor, tuple[int, int]]:
        # data -> ((t), c, h, w), label -> (h, w)
        original_data_shape = data.shape
        image_shape = np.array(original_data_shape[-2:])

        # pad to the smallest multiple of crop_size that contains image_shape.
        patches_per_dim = np.ceil(image_shape / crop_size).astype("int")
        padding_needed = patches_per_dim * crop_size - image_shape

        # (((t) * c) + 1, crop_size * a, crop_size * b) where a, b = patches_per_dim
        prepared_data = self._reshape_and_pad_data(data, label, padding_needed)

        # batch patches
        h1, w1 = patches_per_dim
        h2 = w2 = crop_size
        # data -> (a * b, (t) * c, crop_size, crop_size)
        prepared_data = rearrange(
            prepared_data,
            "tc (h1 h2) (w1 w2) -> (h1 w1) tc h2 w2",
            h1=h1,
            h2=h2,
            w1=w1,
            w2=w2,
        )

        # data -> ((t) * c, crop_size * a, crop_size * b), label -> (crop_size * a, crop_size * b)
        data, label = self._split_prepared_data(prepared_data, dim=1)

        # restore original shape
        data = data.reshape((h1 * w1, *original_data_shape[:-2], h2, w2))

        return data, label, (int(h1), int(w1))

    def __getitem__(self, idx: int) -> DatasetItemT:
        scene_group: zarr.Group = self.root_group[self.scene_names[idx]]
        # data ((t), c, h, w) label (h, w)
        data = torch.from_numpy(scene_group["data"].astype(float)[:]).float()
        label = torch.from_numpy(scene_group["label"].astype(float)[:]).float()

        if self.transforms is not None:
            data = self.transforms(data)

        if (patch_size := self.config.dataset.patch_size) is not None:
            if self.stage != "train":
                data, label, patches_per_dim = self._batch_complete_image(
                    data, label, patch_size
                )
                return data, label, self.scene_names[idx], patches_per_dim
            else:
                random = (self.stage == "train") and self.config.dataset.random_crop
                data, label = self._crop_data(data, label, patch_size, random)
                return data, label, self.scene_names[idx]


class SceneDataModule(pl.LightningDataModule):
    """Class to create train/val/test SceneDataset from a dataset dir.

    Args:
        config: Config class instance.
        run_path: Path where experiment files will be saved.
    """

    def __init__(self, config: Config, run_path: Path | None):
        super().__init__()
        with zarr.LMDBStore(
            str(DATA_SOURCES / f"{config.dataset.dataset_name}" / "scenes.lmdb"),
            readonly=True,
            lock=False,
        ) as data_store:
            root_group = zarr.open_consolidated(data_store)
            scene_group_keys = list(
                filter(
                    None,
                    map(
                        self._filter_function_wrapper(config.dataset.metadata_filter),
                        root_group.items(),
                    ),
                )
            )
            self.image_patches_map = {
                scene_group_name: np.prod(
                    np.ceil(
                        np.array(scene_group.get("label").shape)
                        / config.dataset.patch_size
                    )
                )
                for scene_group_name, scene_group in root_group.groups()
            }

        # exclude testing Scenes
        scene_group_keys, self.test_scene_group_keys = self._split_test_scenes(
            scene_group_keys, config
        )

        # discard percentage of small lakes data
        if (keep_ratio := config.dataset.small_keep_ratio) < 1:
            logger.info(
                f"Experiment will only be using {keep_ratio*100}% "
                f"of the small lakes from non-testing data to Train and Validate."
            )

            small_scenes, big_scenes = partition(
                lambda scene_key: self.image_patches_map[scene_key] > 1,
                scene_group_keys,
            )
            keep_threshold = math.ceil(len(small_scenes) * keep_ratio)
            small_scenes = small_scenes[:keep_threshold]
            scene_group_keys = big_scenes + small_scenes
        self.scene_group_keys = scene_group_keys

        # save metadata about data samples
        if run_path is not None:
            omegaconf.OmegaConf.save(
                {
                    "Number of samples": {
                        "train/val": len(self.scene_group_keys),
                        "test": len(self.test_scene_group_keys),
                    }
                },
                run_path / "dataset_stats.yaml",
            )

        crossval_splitter = KFold(
            n_splits=config.train.cross_val_folds,
            shuffle=True,
            random_state=config.seed,
        )
        self.crossval_splits: dict[int, dict[RunningStage, list[int]]] = {
            i: {
                RunningStage("train"): train_idxs.tolist(),
                RunningStage("validate"): val_idxs.tolist(),
            }
            for i, (train_idxs, val_idxs) in enumerate(
                crossval_splitter.split(self.scene_group_keys)
            )
        }
        self.config = config
        self._fold_iter = iter(range(config.train.cross_val_folds))
        self._fold: int | None = None
        self._datasets: dict[RunningStage, SceneDataset] | None = None
        self._num_workers = config.dataloader.num_workers

    @staticmethod
    def _filter_function_wrapper(
        metadata_filter: dict[str, tuple[str, ...]] | None
    ) -> Callable[[tuple[str, zarr.Group]], str | None]:
        def filter_function(group_item: tuple[str, zarr.Group]) -> str | None:
            group_name, group = group_item
            if metadata_filter is None:
                return group_name
            for metadata_item_name, possible_values in metadata_filter.items():
                if (
                    group_attr := group.attrs.get(metadata_item_name, None)
                ) is not None:
                    if group_attr in possible_values:
                        return group_name
            return None

        return filter_function

    @staticmethod
    def _split_test_scenes(
        scenes: list[str], config: Config
    ) -> tuple[list[str], tuple[str, ...]]:
        if (test_scenes := config.dataset.test_scenes) is None:
            logger.info("test scenes not provided in config.")
            if not (test_scenes_path := RUNS_LOG_DIR / "test_scenes.yaml").is_file():
                logger.info(
                    "Couldn't find saved test_scenes.yaml\n"
                    "Randomly generating and saving list of Scenes to use for testing."
                )
                scenes, test_scenes = train_test_split(
                    scenes,
                    test_size=0.1,
                    shuffle=True,
                    random_state=config.seed,
                )
                omegaconf.OmegaConf.save(test_scenes, RUNS_LOG_DIR / "test_scenes.yaml")
                return scenes, test_scenes
            else:
                logger.info("Found saved test_scenes.yaml.")
                test_scenes = tuple(omegaconf.OmegaConf.load(test_scenes_path))
        else:
            logger.info("Using test scenes provided in config.")
        scenes = [group_key for group_key in scenes if group_key not in test_scenes]
        return scenes, test_scenes

    @property
    def fold(self) -> int | None:
        """Current Cross validation fold.

        Returns:
            Current fold.
        """
        return self._fold

    def fold_step(self) -> int:
        """Iterate to next cross validation fold.

        Returns:
            Current fold.
        """
        self._fold = next(self._fold_iter, None)
        return cast(int, self._fold)

    def setup(self, stage: str) -> None:  # noqa: D102
        match TrainerFn(stage):
            case "test" | "predict":
                running_stage = RunningStage(stage)
                self._datasets = {
                    running_stage: SceneDataset(
                        scene_names=self.test_scene_group_keys,
                        config=self.config,
                        stage=running_stage,
                    )
                }
            case "fit":
                assert self.fold is not None, (
                    "Setup is called before cross validation fold iteration is started."
                    "Please call fold_step() before setting up datasets."
                )
                train_running_stage = RunningStage("train")
                validate_running_stage = RunningStage("validate")
                self._datasets = {
                    train_running_stage: SceneDataset(
                        scene_names=[
                            self.scene_group_keys[idx]
                            for idx in self.crossval_splits[self.fold][
                                train_running_stage
                            ]
                        ],
                        config=self.config,
                        stage=train_running_stage,
                    ),
                    validate_running_stage: SceneDataset(
                        scene_names=[
                            self.scene_group_keys[idx]
                            for idx in self.crossval_splits[self.fold][
                                validate_running_stage
                            ]
                        ],
                        config=self.config,
                        stage=validate_running_stage,
                    ),
                }
            case _:
                raise NotImplementedError

    def _get_dataloader(self) -> DataLoader:
        if (trainer := self.trainer) is not None:
            stage = cast(RunningStage, trainer.state.stage)
            assert (
                self._datasets is not None
            ), "Please call setup before using dataloaders"
            batch_size = 1 if stage == "predict" else self.config.dataloader.batch_size
            dataset = self._datasets[stage]
            sampler = (
                WeightedRandomSampler(
                    [
                        self.image_patches_map[scene_name]
                        for scene_name in dataset.scene_names
                    ],
                    num_samples=len(dataset),
                )
                if stage == "train" and self.config.dataloader.weighted_sampler
                else None
            )
            return DataLoader(
                dataset=dataset,
                batch_size=batch_size,
                shuffle=stage == "train" and sampler is None,
                sampler=sampler,
                num_workers=self._num_workers,
                collate_fn=_custom_collate_fn,
                pin_memory=True,
            )
        raise RuntimeError("Trainer not available")

    def train_dataloader(self) -> DataLoader:  # noqa: D102
        return self._get_dataloader()

    def val_dataloader(self) -> DataLoader:  # noqa: D102
        return self._get_dataloader()

    def test_dataloader(self) -> DataLoader:  # noqa: D102
        return self._get_dataloader()

    def predict_dataloader(self) -> DataLoader:  # noqa: D102
        return self._get_dataloader()

    @contextmanager
    def copy(self):
        """Copy self for use on independent runs.

        Takes care of releasing object after use.

        Yields:
            Copy of self.
        """
        self_copy = deepcopy(self)
        try:
            yield self_copy
        finally:
            del self_copy
            gc.collect()
