"""Module with methods to deal with scenes."""

from __future__ import annotations

import logging
import warnings
from collections.abc import Iterator
from typing import Annotated

import cyclopts
import hydra
import multiprocess
import numpy as np
import pyproj
import pystac_client.exceptions
import rasterio
import rasterio.errors
import rasterio.transform
import tiledb
import tiledb.cf
import tqdm
import xarray
from multiprocess.queues import SimpleQueue
from odc.stac import stac_load
from pystac import Item
from pystac_client import Client
from rasterio.features import rasterize

from codigo.configs import DatasetPreprocessConfig
from codigo.data.data_source_readers import DataSource, DataSources
from codigo.data.geometry import Geometry
from codigo.paths import CONFIG_DIR

logger = logging.getLogger(__name__)


class SceneCreator:
    """Scene creator class."""

    def __init__(
        self,
        config: DatasetPreprocessConfig,
    ):
        self.config = config
        self.catalog = Client.open(config.catalog)

    def _query_items(self, geometry: Geometry) -> Iterator[Item]:
        return self.catalog.search(
            max_items=10,
            collections=self.config.collections,
            bbox=geometry.shape.bounds,
            # intersects=geometry.shape,
            datetime=self.config.tolerance.interval_from(geometry.id.date),
        ).items()

    def _get_stack_scene(self, geometry: Geometry) -> xarray.DataArray | None:
        try:
            bands = list(self.config.bands.keys())
            return (
                stac_load(
                    self._query_items(geometry),
                    bands=bands,
                    groupby="solar_day",
                    crs="utm",
                    resolution=self.config.resolution,
                    geopolygon=geometry.shape,
                    pool=len(bands),
                    stac_cfg={
                        collection: {
                            "assets": {
                                "*": {"nodata": -1},
                            },
                        }
                        for collection in self.config.collections
                    },
                )
                .rename_vars(self.config.bands)
                .to_array(dim="bands", name="input")
            ).drop_vars(("x", "y"))
        except (ValueError, rasterio.errors.RasterioIOError):
            return None
        except pystac_client.exceptions.APIError as e:
            logger.warning(f"Scene {geometry.id}\n" + str(e))
            return None

    def _rasterize_like(
        self, stac_scene: xarray.DataArray, geometry: Geometry
    ) -> xarray.DataArray:
        x, y = stac_scene.x, stac_scene.y
        shape = (y.size, x.size)
        transform = rasterio.transform.from_origin(
            x.min(), y.max(), self.config.resolution, self.config.resolution
        )
        shp_raster = rasterize(
            [
                (
                    geometry.with_crs(
                        pyproj.CRS(f"EPSG:{stac_scene.spatial_ref.item()}")
                    ).shape,
                    1,
                )
            ],
            out_shape=shape,
            transform=transform,
            fill=0,
            all_touched=True,
        )
        return xarray.DataArray(shp_raster, name="label", dims=["y", "x"])

    def _array_from_geometry(
        self, geometry: Geometry, tiledb_root_uri: str
    ) -> tuple[xarray.Dataset, str] | None:
        geometry = geometry.with_crs(pyproj.CRS("EPSG:4326"))
        with warnings.catch_warnings(
            action="ignore", category=rasterio.errors.NotGeoreferencedWarning
        ):
            # (bands: (t, c, h, w), time: (t, ))
            if (stac_scene := self._get_stack_scene(geometry)) is not None:
                if not self.config.timeseries:
                    stac_scene = stac_scene.sel(
                        time=[
                            stac_scene.time.sel(
                                time=geometry.id.date, method="nearest"
                            ).data
                        ]
                    )
                complete_dataset = (
                    xarray.merge(
                        [
                            stac_scene,
                            self._rasterize_like(stac_scene, geometry),
                        ],
                        combine_attrs="drop",
                    )
                    .drop_vars("spatial_ref")
                    .chunk(  # TODO: Needed?
                        {
                            "time": stac_scene.time.size,
                            "bands": stac_scene.bands.size,
                            "x": stac_scene.x.size,
                            "y": stac_scene.y.size,
                        }
                    )
                    .transpose("time", "bands", "x", "y")
                )

                complete_dataset = complete_dataset.expand_dims(
                    dim={"scenes": np.array([geometry.id.to_string()])}
                )

                if (metadata := geometry.metadata) is not None:
                    for metadata_name, metadata_value in metadata.items():
                        complete_dataset.attrs[metadata_name] = metadata_value

                for var in complete_dataset.variables:
                    complete_dataset[var].encoding["compressor"] = None

                # tiledb doesn't accept object types
                complete_dataset["bands"] = complete_dataset["bands"].astype(str)
                complete_dataset["scenes"] = complete_dataset["scenes"].astype(str)

                return (
                    complete_dataset.compute(),
                    f"{tiledb_root_uri}/{geometry.id.to_string()}",
                )
        return None

    @staticmethod
    def _save_to_db(queue: SimpleQueue):
        # print("started db writer", flush=True)
        ctx = tiledb.Ctx()
        while True:
            if (result := queue.get()) is None:
                break
            dataset, group_uri = result
            tiledb.cf.from_xarray(dataset, group_uri=group_uri, ctx=ctx)

    def from_data_source(self, data_source: DataSource):
        """Acquire data for data source and save it in a database.

        Args:
            data_source: Data source object to obtain data for.
        """
        tiledb_root_uri = data_source.geometry_dir.parent / "scenes"
        if not tiledb_root_uri.is_dir():
            tiledb.Group.create(str(tiledb_root_uri), tiledb.Ctx())

        # for data in tqdm.tqdm(data_source, desc="Downloading"):
        #    partial(self._array_from_geometry, tiledb_root_uri=str(tiledb_root_uri), tiledb_ctx=tiledb_ctx)(data)

        ctx = multiprocess.context.SpawnContext()

        queue = ctx.SimpleQueue()
        db_process = ctx.Process(target=self._save_to_db, args=(queue,))
        db_process.start()

        bar = tqdm.tqdm()

        def _callback(result: tuple[xarray.Dataset, str] | None):
            if result is not None:
                queue.put(result)
                bar.update()

        with ctx.Pool(processes=self.config.num_workers) as pool:
            for geometry in data_source:
                pool.apply_async(
                    self._array_from_geometry,
                    args=(geometry, tiledb_root_uri),
                    callback=_callback,
                )
            pool.close()
            pool.join()
        queue.put(None)
        db_process.join()


data_app = cyclopts.App(name="data")


@data_app.command()
def preprocess(
    data_source_name: Annotated[
        DataSources, cyclopts.Parameter()
    ] = DataSources.highasiagl2018,
    overrides: Annotated[list[str] | None, cyclopts.Parameter()] = None,
) -> None:
    """Run downloading and preprocessing for dataset.

    Args:
        data_source_name: Name of dataset to preprocess.
        overrides: Overrides for preprocessing config.
    """
    with hydra.initialize_config_dir(str(CONFIG_DIR), version_base=None):
        conf = hydra.compose(config_name="preprocess", overrides=overrides)
    data_source: DataSource = data_source_name.value()
    SceneCreator(hydra.utils.instantiate(conf)).from_data_source(data_source)
