"""Utility functions for data."""

import torch
from beartype.door import is_bearable
from torch.utils.data import default_collate

CutDatasetItemT = tuple[torch.Tensor, torch.Tensor, str]
CompDatasetItemT = tuple[torch.Tensor, torch.Tensor, str, tuple[int, int]]

DatasetItemT = CutDatasetItemT | CompDatasetItemT

CutBatchT = tuple[torch.Tensor, torch.Tensor, list[str]]
CompBatchT = tuple[torch.Tensor, torch.Tensor, list[str], list[tuple[int, int]]]

BatchT = CutBatchT | CompBatchT


def _custom_collate_fn(
    batch: list[CutDatasetItemT] | list[CompDatasetItemT],
) -> CutBatchT | CompBatchT:
    if is_bearable(batch, list[CutDatasetItemT]):
        split_stackables = map(lambda item: (item[:2], item[2]), batch)
        data_labels, data_idx = tuple(map(list, zip(*split_stackables)))
        data, labels = default_collate(data_labels)
        return data, labels, data_idx
    elif is_bearable(batch, list[CompDatasetItemT]):
        data, labels, data_idx, patches_per_dim = tuple(map(list, zip(*batch)))
        return torch.vstack(data), torch.vstack(labels), data_idx, patches_per_dim
    raise NotImplementedError
