"""Module to create data sources classes, i.e. shape file to Geometry mappers."""

import datetime
import logging
from abc import ABC, abstractmethod
from collections.abc import Iterable, Iterator
from enum import Enum
from functools import partial, reduce
from itertools import chain
from pathlib import Path
from typing import NamedTuple

import pyproj
import shapely
from shapefile import Reader, ShapeRecord

from codigo.data.geometry import Geometry, GeometryId
from codigo.paths import DATA_SOURCES

logger = logging.getLogger(__name__)


GeometryDict = dict[int, Geometry]


class GeometryProperties(NamedTuple):
    """Class to hold all geometry properties.

    Args:
        id: GeometryId instance.
        metadata: Dictionary of name and
         string value for extra geometry metadata.
    """

    id: GeometryId
    metadata: dict[str, str] | None


class DataSource(ABC, Iterable[Geometry]):
    """Base class for creating dataset sources.

    This class can be used to build new data sources, i.e.
        map shape files to the Geometry structure. This is done
        by implementing a property_mapper function that is
        specific to the data of the shapefiles the data source
        will be built from.
    The base class should be able to cope with all shapes being in
        one shape file or in separate shape files in the data directory.
        If the latter is true, disjoint shapes that cover the same object
        should be in the same file.

    Args:
        data_source_path: Path to data source.
            It presumes the existence of subdir geometries.
        property_fields: Tuple of field names one is interested in
            extracting from the shp files.
    """

    def __init__(self, data_source_path: Path, property_fields: tuple[str, ...]):
        super().__init__()
        self.geometry_dir = data_source_path / "geometries"
        self.property_fields = property_fields

    @abstractmethod
    def property_mapper(
        self, property_fields_dict: dict[str, str]
    ) -> GeometryProperties | None:
        """Abstract method that maps properties from shape file to GeometryProperties.

        This method is implemented in a way that maps the property fields
            found in the shape files into GeometryProperties. GeometryProperties
            is composed of GeometryId, with fields id (unique identifier of shape)
            and date (date of shape segmentation) and metadata, a dictionary (str, str)
            of extra information about the shapes. Bear in mind this function expects all
            shapes' fields to be a super set of property_fields.

        Args:
            property_fields_dict: Dictionary of field names and values for each shape's
                properties information.

        Returns:
            GeometryProperties if file contains needed information and None otherwise.
        """
        #  TODO: Allow shape with varying metadata schemas
        raise NotImplementedError

    def _shaperecord_processor(
        self,
        acc_dict: GeometryDict,
        shape_record: ShapeRecord,
        crs: pyproj.CRS,
        fields: tuple[str],
    ) -> GeometryDict:
        field_dict = {
            property_field: shape_record.record[fields.index(property_field)]
            for property_field in self.property_fields
        }
        if (geo_props := self.property_mapper(field_dict)) is not None:
            if shape := shapely.geometry.shape(shape_record.shape):
                if acc_geometry := acc_dict.get(hash(geo_props.id)):
                    acc_geometry.shape = shapely.union(shape, acc_geometry.shape)
                    assert (
                        geo_props.metadata == acc_geometry.metadata
                    ), "Disjoint shapes of same objects have different shape specific metadata"
                else:
                    acc_dict[hash(geo_props.id)] = Geometry(
                        geo_props.id, shape, crs, geo_props.metadata
                    )
        return acc_dict

    @staticmethod
    def _get_file_crs(file_path: Path) -> pyproj.CRS | None:
        projection_path = file_path.with_suffix(".prj")
        if projection_path.is_file():
            with open(projection_path) as f:
                if f.readable():
                    return pyproj.CRS(f.read())
            logger.warning(f"Projection file {str(projection_path)} is not readable.")
        else:
            logger.warning(f"File {str(file_path)} doesn't have a projection.")
        return None

    def _geometry_from_file(self, file_path: Path) -> list[Geometry]:
        reader = Reader(file_path)
        fields = tuple(map(lambda field: field[0], reader.fields[1:]))
        assert set(self.property_fields).issubset(fields)

        geometry_dict: dict[int, Geometry] = reduce(
            partial(
                self._shaperecord_processor,
                crs=self._get_file_crs(file_path),
                fields=fields,
            ),
            reader.iterShapeRecords(),
            {},
        )

        return list(geometry_dict.values())

    def __iter__(self) -> Iterator[Geometry]:
        return chain.from_iterable(
            map(self._geometry_from_file, self.geometry_dir.glob("*.shp"))
        )


class HighAsiaGL(DataSource):
    """Data source class for data from highasiagl2018.

    Reference:
        https://data.tpdc.ac.cn/zh-hans/data/862a34a4-b816-4a49-b218-302db124ca84/
    """

    def __init__(self):
        super().__init__(
            data_source_path=DATA_SOURCES / "highasiagl2018",
            property_fields=("GLAKE_ID", "GL_Image", "GL_Type"),
        )

    @staticmethod
    def _map_lake_type(lake_type: str) -> str:
        match lake_type:
            case "Non-Glacier-Fed Lake":
                return "Non-Glacier"
            case "Glacier-Fed Lake":
                return "Glacier"
            case _:
                raise NotImplementedError

    def property_mapper(
        self, property_fields_dict: dict[str, str]
    ) -> GeometryProperties | None:
        """Map GLAKE_ID to id, GL_Image to date and GL_Type to lake_type.

        Args:
            property_fields_dict: Dictionary of field names and values for each shape's
                properties information.

        Returns:
            GeometryProperties if file contains needed information and None otherwise.
        """
        if len(image_name := property_fields_dict["GL_Image"].split("_")) == 5:
            date = image_name[3]
            lake_type = self._map_lake_type(property_fields_dict["GL_Type"])
            return GeometryProperties(
                GeometryId(
                    property_fields_dict["GLAKE_ID"],
                    datetime.datetime.strptime(date, "%Y%m%d"),
                ),
                {"lake_type": lake_type},
            )
        else:
            return None


class DataSources(Enum):
    """List of possible data sources."""

    highasiagl2018 = HighAsiaGL


if __name__ == "__main__":
    print(list(HighAsiaGL()))
