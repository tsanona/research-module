"""Module with methods to deal with geometries and shape files."""

from __future__ import annotations

import logging
from dataclasses import dataclass
from datetime import datetime
from typing import ClassVar, TypeAlias

import pyproj
import shapely
from shapely.ops import transform

logger = logging.getLogger(__name__)


@dataclass
class GeometryId:
    """Class to hold Geometry identifiers.

    Args:
        name: Geometry id.
        date: Geometry date of creation.
    """

    name: str
    date: datetime
    DATE_FORMAT: ClassVar[str] = "%Y%m%d"

    def __hash__(self):
        return hash((self.name, self.date))

    def to_string(self) -> str:
        """Get id string from params.

        Returns:
            String with form <name>_<date>,
            where date is formatted as "%Y%m%d"
        """
        return "_".join((self.name, self.date.strftime(self.DATE_FORMAT)))

    @classmethod
    def from_string(cls, string: str) -> GeometryId:
        """Get class from id string.

        Args:
            string: String with id information.
                Should have the form <name>_<date>,
                where date is formatted as "%Y%m%d"

        Returns:
            GeometryId class.
        """
        name, date = string.split("_")
        return cls(name, datetime.strptime(date, cls.DATE_FORMAT))


PolygonT: TypeAlias = shapely.Polygon | shapely.MultiPolygon


@dataclass
class Geometry:
    """Class to hold all geometry components.

    Args:
        id: Geometry id object.
        shape: Shapely Polygon/MultiPolygon object.
        crs: Pyproj CRS object
        metadata: Dict with geometry specific metadata.
    """

    id: GeometryId
    shape: PolygonT
    crs: pyproj.CRS
    metadata: dict[str, str] | None

    @staticmethod
    def _get_transformer(in_crs: pyproj.CRS, out_crs: pyproj.CRS) -> pyproj.Transformer:
        return pyproj.Transformer.from_crs(in_crs, out_crs, always_xy=True)

    def with_crs(self, crs: pyproj.CRS) -> Geometry:
        """Reproject Geometry to crs.

        Args:
            crs: Crs to reproject geometry to.

        Returns:
            Geometry object with reprojected to crs.
        """
        trans = self._get_transformer(self.crs, crs)
        self.shape = transform(trans.transform, self.shape)
        self.crs = crs
        return self
