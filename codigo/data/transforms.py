"""Data transformations."""

from functools import cache

import numpy as np
import torch
import torch.nn as nn


class SliceBands(nn.Module):
    """Get a slice of the original bands.

    Args:
        idx: Indexes of bands to get for slice.
        drop: If True, drop idx and return all other
        bands as slice.
    """

    def __init__(self, idx: tuple[int, ...], drop: bool):
        super().__init__()
        self.idx = idx
        self.drop = drop

    @cache
    def _resolve_idx(self, n_bands: int) -> tuple[int, ...]:
        if self.drop:
            return tuple(np.setdiff1d(np.arange(n_bands), self.idx))
        return self.idx

    def forward(self, x: torch.Tensor) -> torch.Tensor:  # noqa: D102
        idx = self._resolve_idx(x.shape[-3])
        return x[..., idx, :, :]


class Normalize(nn.Module):
    """Normalize by factor.

    Args:
        normalization_factor: Factor to multiply data by.
    """

    def __init__(self, normalization_factor: float):
        super().__init__()
        self.normalization_factor = normalization_factor

    def forward(self, x: torch.Tensor) -> torch.Tensor:  # noqa: D102
        return x * self.normalization_factor

    def revert(self, x: torch.Tensor) -> torch.Tensor:
        """Denormalize data.

        Args:
            x: data to revert normalization on.

        Returns:
            Denormalized data.
        """
        return x / self.normalization_factor
