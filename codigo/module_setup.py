"""Module setup functions."""

from pathlib import Path

import lightning.pytorch as pl
import optuna as op
from lightning.pytorch.callbacks import (  # noqa: F401
    EarlyStopping,
    LearningRateMonitor,
    ModelCheckpoint,
    ModelSummary,
    TQDMProgressBar,
)
from lightning.pytorch.loggers import TensorBoardLogger
from lightning.pytorch.profilers import AdvancedProfiler  # noqa: F401
from omegaconf import DictConfig
from torchmetrics import (
    Accuracy,
    F1Score,
    JaccardIndex,
    MeanAbsoluteError,
    MetricCollection,
)

from codigo.callbacks import LogHyperparamBestMetricCallback, OptunaPruningCallback
from codigo.configs import Config
from codigo.data.datasets_datamodules import SceneDataModule
from codigo.models.lightningmodules import Resnet50


def setup_trainer(
    config: Config,
    run_path: Path,
    trial: op.Trial | None,
    hparams: DictConfig | None,
    predicting: bool,
) -> pl.Trainer:
    """Setup Pytorch Lightning Trainer.

    Args:
        config: Config class instance.
        run_path: Path of the run setting up for.
        trial: Optuna trial instance if run is
            hypertune else None.
        hparams: Dictionary with Hyper parameters values.
        predicting: Weather trainer will be used for predicting.

    Returns:
        Pytorch Lightning Trainer.
    """
    logger = TensorBoardLogger(
        save_dir=run_path.parent,
        name=run_path.name,
        version=f"trial_{trial.number}" if trial else "training",
        default_hp_metric=False,
    )

    callbacks: list[pl.Callback] = [
        # EarlyStopping(monitor="loss/val", verbose=True, patience=20),
        TQDMProgressBar(),
    ]
    if config.lr_scheduler is not None:
        callbacks.append(LearningRateMonitor(logging_interval="epoch"))
    if hparams is not None:
        callbacks.append(
            LogHyperparamBestMetricCallback(hparams=hparams, config=config)
        )
    if trial is not None:
        callbacks.append(
            OptunaPruningCallback(trial, f"{config.hypertune.objective_metric}/val")
        )
        if trial.number == 0:
            callbacks.append(ModelSummary(max_depth=3))
    elif not predicting:
        callbacks.append(
            ModelCheckpoint(
                dirpath=run_path / "training",
                filename="training@epoch{epoch}",
                monitor="loss/val",
                auto_insert_metric_name=False,
            )
        )
        callbacks.append(ModelSummary(max_depth=3))

    return pl.Trainer(
        accelerator=config.device,
        logger=None if predicting else logger,
        callbacks=callbacks,
        max_epochs=config.train.epochs if trial is None else config.hypertune.epochs,
        num_sanity_val_steps=0,
        log_every_n_steps=1,
        enable_checkpointing=not trial,
        enable_model_summary=False,
        # profiler=AdvancedProfiler(run_path) if trial is not None and trial.number == 0 else None
    )


def setup_model(config: Config, hparams: DictConfig | None) -> pl.LightningModule:
    """Setup Pytorch Lightning Module.

    Args:
        config: Config class instance.
        hparams: Dictionary with Hyper parameters values.

    Returns:
        Pytorch Lightning Module.
    """
    metrics = MetricCollection(
        {
            "MAE": MeanAbsoluteError().to(config.device),
            "Accuracy": Accuracy(task="binary").to(config.device),
            "F1Score": F1Score(task="binary").to(config.device),
            "JaccardIndex": JaccardIndex(task="binary").to(config.device),
        }
    )

    return Resnet50(
        config=config,
        metrics=metrics,
        hparams=hparams,
    )


def setup_data(config: Config, run_path: Path | None) -> SceneDataModule:
    """Setup Pytorch Lightning Data Module.

    Args:
        config: Config class instance.
        run_path: Path where experiment files will be saved.

    Returns:
        Pytorch Lightning Data Module.
    """
    return SceneDataModule(config, run_path)


def setup_modules(
    config: Config,
    run_path: Path,
    trial: op.Trial | None,
    hparams: DictConfig | None,
    predicting: bool,
) -> tuple[pl.Trainer, pl.LightningModule]:
    """Setup Trainer and Lightning Module.

    Args:
        config: Config class instance.
        run_path: Path of the run setting up for.
        trial: Optuna trial instance if run is
            hypertune else None.
        hparams: Dictionary with Hyper parameters values.
        predicting: Weather trainer will be used for predicting.

    Returns:
        Tuple with Trainer and Lightning Module.
    """
    return (
        setup_trainer(config, run_path, trial, hparams, predicting),
        setup_model(config, hparams),
    )


def setup_optuna(config: Config, run_path: Path) -> op.Study:
    """Setup Optuna Study.

    Args:
        config: Config class instance.
        run_path: Path where running artifacts are stored.

    Returns:
        Optuna Study instance.
    """
    tuning_storage_path = run_path / "storage.db"
    return op.create_study(
        storage=f"sqlite:///{tuning_storage_path}",
        sampler=config.hypertune.sampler.instantiate(),
        pruner=config.hypertune.pruner.instantiate(),
        study_name=run_path.name,
        direction=config.hypertune.direction,
        load_if_exists=True,
    )
