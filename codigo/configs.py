"""Configuration classes."""

from __future__ import annotations

import logging
import math
import operator
import sys
from abc import ABC, abstractmethod
from collections import defaultdict
from collections.abc import Iterable
from copy import deepcopy
from datetime import datetime, timedelta
from functools import reduce
from itertools import starmap
from pathlib import Path
from typing import Any, Literal, TypeVar, cast

import hydra.utils
import numpy as np
import optuna as op
import segmentation_models_pytorch as smp
import torch
import torch.nn as nn
from omegaconf import DictConfig, OmegaConf
from pydantic import (
    BaseModel,
    PositiveInt,
    field_validator,
    model_serializer,
    model_validator,
)

from codigo import losses
from codigo.data import transforms
from codigo.models.utils import SatelliteResnet50Encoder
from codigo.paths import DATA_DIR, ROOT_DIR

logger = logging.getLogger(__name__)


ParamT = TypeVar("ParamT", str, float, int)


class BaseModelWithSerializedTarget(BaseModel):
    """Wrapper of Pydantic BaseModel to serialize hydra instantiable dicts."""

    @model_serializer(mode="wrap")
    def _serialize_model(self, handler, info):
        model_dump = handler(self, info)
        if extra_fields := self.model_fields_set.difference(model_dump.keys()):
            model_dump.update(
                {field_name: getattr(self, field_name) for field_name in extra_fields}
            )
        class_file_path = Path(
            sys.modules[self.__class__.__module__].__file__
        ).relative_to(ROOT_DIR)
        class_import_str = (
            str(class_file_path / self.__class__.__name__)
            .replace(".py", "")
            .replace("/", ".")
        )
        model_dump.update({"_target_": class_import_str})
        return model_dump


class Tolerance(BaseModelWithSerializedTarget):
    """Configuration for temporal tolerance of data aquisition.

    Args:
        time: Time divisions in int equal to timedelta.
        type: Type of tolerance.
            Weather to apply tolerance "symmetrically" or only before segmentation date.
    """

    time: dict[str, int]
    type: Literal["symmetric", "before"]

    @property
    def time_delta(self) -> timedelta:
        return timedelta(**self.time)

    def interval_from(self, timestamp: datetime) -> tuple[datetime, datetime]:
        match self.type:
            case "symmetric":
                multiplier = [-1, 1]
            case "before":
                multiplier = [-1, 0]
            case _:
                raise NotImplementedError
        return tuple(
            np.full(2, timestamp) + np.multiply(multiplier, np.full(2, self.time_delta))
        )


class DatasetPreprocessConfig(BaseModelWithSerializedTarget):
    """Configuration for data preprocessing.

    Args:
        catalog: STAC catalog url.
        collections: Collections to use from catalog.
        bands: Map for band names used in collection to "standard" band names.
            See documentation for standard band names.
        tolerance:  Days of tolerance between segmentation data and data date
        resolution: Resolution of images in final product.
        length_of_series: Length of timeseries for each image.
    """

    catalog: str
    collections: list[str]
    bands: dict[str, str]
    tolerance: Tolerance
    resolution: int
    length_of_series: PositiveInt
    num_workers: int


class ClassConstructorModel(ABC, BaseModelWithSerializedTarget):
    """Generalized model that constructs other classes.

    Obtain Classes based on name and parameters.
    Args:
        name: Name of optimizer class to be used.
        params: Dict with parameters
            and their values for optimizer
    """

    name: str
    params: dict[str, Any] | None

    @field_validator("name")
    @classmethod
    def _validate_name(cls, name: str) -> str:
        return cls._assert_exists(name)

    @field_validator("params")
    @classmethod
    def _none_to_empty_dict(cls, params: dict[str, Any] | None) -> dict[str, Any]:
        if params is None:
            return defaultdict(None)
        return params

    @classmethod
    @abstractmethod
    def _assert_exists(cls, name: str) -> str:
        raise NotImplementedError


class OptimizerConfig(ClassConstructorModel):
    """Default config of optimizer params."""

    @classmethod
    def _assert_exists(cls, name: str) -> str:
        if not hasattr(torch.optim, name):
            raise ValueError(
                f"{name} is not a valid optimizer."
                f"Please choose from Algorithms in https://pytorch.org/docs/stable/optim"
            )
        return name

    def instantiate(self, params: Iterable[torch.Tensor]) -> torch.optim.Optimizer:
        """Instantiate optimizer module.

        Args:
            params: Model parameters to be optimized.

        Returns:
            torch optim Optimizer module.
        """
        return getattr(torch.optim, self.name)(params=params, **self.params)


class LRSchedulerConfig(ClassConstructorModel):
    """Default config of lr scheduler params."""

    @classmethod
    def _assert_exists(cls, name: str) -> str:
        if not hasattr(torch.optim.lr_scheduler, name):
            raise ValueError(
                f"{name} is not a valid Learning rate scheduler."
                f"Please choose lr_scheduler in https://pytorch.org/docs/stable/optim"
            )
        return name

    def instantiate(
        self, optimizer: torch.optim.Optimizer
    ) -> torch.optim.lr_scheduler.LRScheduler:
        """Instantiate learning rate scheduler module.

        Args:
            optimizer: Model parameters to be optimized.

        Returns:
            torch optim learning rate scheduler module.
        """
        return getattr(torch.optim.lr_scheduler, self.name)(
            optimizer=optimizer, **self.params
        )


class DataTransformConfig(ClassConstructorModel):
    """Default config of data transform params."""

    @classmethod
    def _assert_exists(cls, name: str) -> str:
        if not hasattr(transforms, name):
            raise ValueError(
                f"{name} is not a valid data transform."
                f"Please choose data transform from codigo/data/transforms.py"
            )
        return name

    def instantiate(self) -> nn.Module:
        """Instantiate data transform module.

        Returns:
            torch nn module.
        """
        return getattr(transforms, self.name)(**self.params)


class DatasetConfig(BaseModelWithSerializedTarget):
    """Default config of dataset params.

    Args:
        dataset_name: Dataset name.
        patch_size: Size of (square) patch used in from data.
        random crop: If True, a patch of patch_size is cropped
            randomly from the original image. Otherwise, the cut is done
            from the center of the image. Is only applied to train dataloader.
        data_transforms: Config of transformations to apply to data in dataset but NOT to labels.
        metadata_filter: Filter to be applied to scene metadata in dataset.
        test_scenes: List of scene names used for testing. If non are provided,
            test scenes are automatically selected from all data with a ratio of 0.1.
        small_keep_ratio: Ratio of kept "small" samples
    """

    dataset_name: str
    patch_size: int
    padding_type: Literal["tight", "patch_size"]
    random_crop: bool
    data_transforms: dict[str, DataTransformConfig] | None
    metadata_filter: dict[str, tuple[str, ...]] | None
    test_scenes: tuple[str, ...] | None
    small_keep_ratio: float


class DataLoaderConfig(BaseModelWithSerializedTarget):
    """Default config of DataLoader params.

    Args:
        batch_size: Number of samples per batch.
        num_workers: Number of workers used by dataloader
    """

    batch_size: int
    weighted_sampler: bool
    num_workers: int


class TrainConfig(BaseModelWithSerializedTarget):
    """Default config of train params.

    Args:
        cross_val_folds: Number of cross validation folds.
        epochs: Maximum number of epochs run while training.
            Training might be cut short by early stopping.
    """

    cross_val_folds: int
    epochs: int


class SamplerConfig(ClassConstructorModel):
    """Default config of optuna sampler and its params."""

    @classmethod
    def _assert_exists(cls, name: str) -> str:
        if not hasattr(op.samplers, name):
            raise ValueError(
                f"{name} is not a valid Optuna sampler."
                f"Please choose sampler from "
                f"https://optuna.readthedocs.io/en/stable/reference/samplers/index.html"
            )
        return name

    def instantiate(self) -> op.samplers.BaseSampler:
        """Instantiate sampler module.

        Returns:
            Optuna sampler module.
        """
        return getattr(op.samplers, self.name)(**self.params)


class PrunerConfig(ClassConstructorModel):
    """Default config of optuna pruner and its params."""

    @classmethod
    def _assert_exists(cls, name: str) -> str:
        if not hasattr(op.pruners, name):
            raise ValueError(
                f"{name} is not a valid Optuna Prunner."
                f"Please choose pruner from "
                f"https://optuna.readthedocs.io/en/stable/reference/pruners.html"
            )
        return name

    def instantiate(self) -> op.pruners.BasePruner:
        """Instantiate pruner module.

        Returns:
            Optuna pruner module.
        """
        return getattr(op.pruners, self.name)(**self.params)


class HyperTuneConfig(BaseModelWithSerializedTarget):
    """Default config for hyper tuning params.

    Args:
        objective_metric: Metric to tune hyperparameters to.
        total_num_trials: Total number of trial completed in a hypertune run.
            If num_workers > 1 each will execute n_trials/num_workers runs.
        epochs: Maximum number of epochs run while training.
            Training might be cut short by early stopping.
        num_workers: Number of parallel runs.
        sampler: Sampler base configuration.
        pruner: Pruner base config.
    """

    objective_metric: str
    direction: Literal["maximize", "minimize"]
    total_num_trials: int
    epochs: int
    num_workers: int
    sampler: SamplerConfig
    pruner: PrunerConfig

    @field_validator("num_workers")
    @classmethod
    def _assert_params(cls, v: int) -> int:
        assert v > 0
        return v

    @property
    def trials_per_worker(self) -> int:
        """Number of trials to be executed per worker.

        Returns:
            Int number of trials to be executed by a worker.
        """
        return abs(math.ceil(self.total_num_trials / self.num_workers))


class ModelConfig(ClassConstructorModel):
    """General model config class."""

    freeze_encoder: bool

    @abstractmethod
    def _get_model(self) -> nn.Module:
        raise NotImplementedError

    def instantiate(self) -> nn.Module:
        """Instantiate model module.

        Returns:
            Optuna nn module.
        """
        return self._get_model()


class SMPModelConfig(ModelConfig):
    """Config of smp model and its params."""

    bands: list[str]

    @classmethod
    def _assert_exists(cls, name: str) -> str:
        if not hasattr(smp, name):
            raise ValueError(
                f"{name} is not a valid Segmentation Models Pytorch model."
                f"Please choose model from https://smp.readthedocs.io/en/latest/models.html"
            )
        return name

    @field_validator("params")
    @classmethod
    def _assert_params(cls, v: dict[str, Any]) -> dict[str, Any]:
        if "encoder_weights" not in v.keys():
            v["encoder_weights"] = None
        return v

    def model_post_init(self, __context: Any) -> None:  # noqa: D102
        if (
            self.params is not None
            and self.params["encoder_name"] == "satelliteresnet50"
        ):
            encoder_name = "satelliteresnet50"
            smp.encoders.encoders[encoder_name] = deepcopy(
                smp.encoders.resnet_encoders["resnet50"]
            )
            smp.encoders.encoders[encoder_name]["encoder"] = SatelliteResnet50Encoder
            smp.encoders.encoders[encoder_name]["pretrained_settings"] = {
                "sen12ms": {"url": f"file://{DATA_DIR}/{encoder_name}/s2_weights.pth"}
            }
            smp.encoders.encoders[encoder_name]["params"]["bands"] = self.bands

    def _get_model(self) -> nn.Module:
        return getattr(smp, self.name)(**self.params)


class LossFnConfig(ClassConstructorModel):
    """General loss function config class."""

    @classmethod
    def _assert_exists(cls, name: str) -> str:
        if not hasattr(losses, name):
            raise ValueError(f"{name} is not a valid loss function.")
        return name

    def _get_loss_fn(self) -> nn.Module:
        loss_class = getattr(losses, self.name)
        return loss_class(**self.params)

    def instantiate(self) -> nn.Module:
        """Instantiate loss function module.

        Returns:
            Optuna nn module.
        """
        return self._get_loss_fn()


class PytorchLossFnConfig(LossFnConfig):
    """Config of pytorch loss function and its params."""

    @classmethod
    def _assert_exists(cls, name: str) -> str:
        if not hasattr(torch.nn, name):
            raise ValueError(
                f"{name} is not a valid Pytorch loss function."
                f"Please choose loss from from "
                f"https://pytorch.org/docs/stable/nn.html#loss-functions"
            )
        return name

    def _get_loss_fn(self) -> nn.Module:
        loss_class = getattr(torch.nn, self.name)
        return loss_class(**self.params)


class ComboLoss(nn.Module):
    """Module that performs as combined losses.

    Args:
        - loss_configs: Iterable of loss function configs.
    """

    def __init__(
        self, weights: list[float] | None, loss_configs: Iterable[LossFnConfig]
    ):
        super().__init__()
        self.weights = weights
        self.loss_modules = [loss_config.instantiate() for loss_config in loss_configs]

    def forward(  # noqa: D102
        self, input: torch.Tensor, target: torch.Tensor
    ) -> torch.Tensor:

        return reduce(
            operator.add,
            starmap(
                lambda loss_module, weight: weight * loss_module.forward(input, target),
                zip(self.loss_modules, cast(list[float], self.weights)),
            ),
        )


class ComboLossFnConfig(BaseModelWithSerializedTarget):
    """Config of combination loss."""

    name: str
    weights: dict[str, Any] | None
    losses: dict[str, LossFnConfig]

    @classmethod
    def _assert_exists(cls, name: str) -> str:
        return name

    def _get_loss_fn(self) -> nn.Module:
        if self.weights is None:
            weights = [1.0] * len(self.losses)
        else:
            if len(self.weights) != len(self.losses):
                assert len(self.weights) + 1 == len(self.losses), (
                    "At least 1-#losses weights should be provided. "
                    "They will be mapped for each loss in the same order."
                )
                missing_loss_weight_name = (
                    set(self.losses.keys()).difference(self.weights.keys()).pop()
                )
                self.weights.update(
                    {missing_loss_weight_name: abs(1 - sum(self.weights.values()))}
                )
                assert sum(self.weights.values()) == 1, "Weights should sum up to 1."
            weights = [self.weights[loss_name] for loss_name in self.losses.keys()]
        return ComboLoss(weights, self.losses.values())

    def instantiate(self) -> nn.Module:
        """Instantiate loss function module.

        Returns:
            Optuna nn module.
        """
        return self._get_loss_fn()


HyperparameterT = int | float | bool | str
ChoicesT = tuple[HyperparameterT, ...]


class HyperParameterChoice(BaseModelWithSerializedTarget):
    """Class to hold hyperparameter choices."""

    choices: ChoicesT

    @staticmethod
    def _get_suggestion_type(
        trial: op.Trial, hparam_name: str, suggestion_range: ChoicesT
    ) -> HyperparameterT:
        match suggestion_range:
            case (range_min, range_max) if isinstance(range_min, float) and isinstance(
                range_max, float
            ):
                return trial.suggest_float(hparam_name, range_min, range_max)
            case (range_min, range_max) if isinstance(range_min, int) and isinstance(
                range_max, int
            ):
                return trial.suggest_int(hparam_name, range_min, range_max)
            case suggestion_range if isinstance(suggestion_range, tuple):
                return cast(
                    HyperparameterT,
                    trial.suggest_categorical(hparam_name, suggestion_range),
                )
            case _:
                raise NotImplementedError

    def get_suggestion(self, trial: op.Trial, hparam_name: str) -> HyperparameterT:
        """Obtain hparam suggestion for trial.

        Args:
            trial:  Optuna trial instance
            hparam_name: Name of hyperparameter to obtain suggestion for.

        Returns:
            Suggested value for hyperparameter.
        """
        return self._get_suggestion_type(trial, hparam_name, self.choices)


class Config(BaseModelWithSerializedTarget):
    """Default config of experiment.

    Args:
        seed: random state seed.
        device: Device to perform computations on.
        model: Config class for model.
        loss_fn: Config class for loss.
        optimizer: Config class for optimizer.
        lr_scheduler: Config class for lr scheduler.
        dataset: Config class for dataset.
        dataloader: Config class for dataloader.
        train: Config class for training.
        hypertune: Config class for hyperparameter tuning.

    """

    seed: int
    device: str
    model: ModelConfig
    loss_fn: LossFnConfig | ComboLossFnConfig
    optimizer: OptimizerConfig
    lr_scheduler: LRSchedulerConfig | None
    dataset: DatasetConfig
    dataloader: DataLoaderConfig
    train: TrainConfig
    hypertune: HyperTuneConfig

    @model_validator(mode="before")
    @classmethod
    def _check_device_omitted(cls, data: DictConfig) -> DictConfig:
        if data.get("device") is None:
            data["device"] = (
                "cuda"
                if torch.cuda.is_available()
                else "mps" if torch.backends.mps.is_available() else "cpu"
            )
        return data

    def get_trial_config(
        self, hparam_choices: DictConfig, trial: op.Trial
    ) -> tuple[Config, DictConfig]:
        """Get values for hyperparameter tuning trial.

        Args:
            hparam_choices: DictConfig mapping hparams to HyperParameterChoice instances.
            trial: Optuna trial object.

        Returns:
            Config class for hyperparameter tuning trial.

        Raises:  # noqa: DAR402
            ValueError: If DictConfig item cannot be resolved.
        """

        def _resolve_choices(
            choice_dict: DictConfig,
        ) -> DictConfig:
            hparams = DictConfig({})
            for item_name, value in choice_dict.items():
                match value:
                    case HyperParameterChoice():
                        hparams[item_name] = cast(
                            HyperParameterChoice, value
                        ).get_suggestion(trial, str(item_name))
                    case DictConfig():
                        hparams[item_name] = _resolve_choices(cast(DictConfig, value))
                    case _:
                        raise ValueError(
                            f"Could not resolve item {str(item_name)}, present in hparams"
                        )
            return hparams

        chosen_hparams = _resolve_choices(hparam_choices)
        base_config_dict = OmegaConf.create(self.model_dump())
        return (
            hydra.utils.instantiate(OmegaConf.merge(base_config_dict, chosen_hparams)),
            chosen_hparams,
        )

    def get_tuned_config(
        self, hparam_choices: DictConfig, tuned_hparams: dict[str, HyperparameterT]
    ) -> tuple[Config, DictConfig]:
        """Get values for hyperparameter tuning trial.

        Args:
            hparam_choices: DictConfig mapping hparams to HyperParameterChoice instances.
            tuned_hparams: Dictionary of the tuned hyperparameters.

        Returns:
            Tuned config Class.

        Raises:
            AssertionError: If there is an extra param in either the tuned
                params dict or the param choices dict.

        """

        def _inject_tuned(
            choice_dict: DictConfig,
        ) -> DictConfig:
            hparams = DictConfig({})
            for item_name, value in choice_dict.items():
                if isinstance(value, DictConfig):
                    hparams[item_name] = _inject_tuned(choice_dict[item_name])
                elif (best_value := tuned_hparams.pop(str(item_name))) is not None:
                    hparams[item_name] = best_value
                elif isinstance(value, HyperParameterChoice):
                    raise AssertionError(
                        f"Hparam config contains extra parameter {str(item_name)}"
                    )
            return hparams

        injected_hparams = _inject_tuned(hparam_choices)
        assert not tuned_hparams, f"Study contains extra parameter(s) {tuned_hparams}"
        base_config_dict = OmegaConf.create(self.model_dump())
        return (
            hydra.utils.instantiate(
                OmegaConf.merge(base_config_dict, injected_hparams)
            ),
            injected_hparams,
        )
