"""CLI setup."""

import logging

import cyclopts

from codigo.data.scenes import data_app
from codigo.run import experiments_app

logger = logging.getLogger(__name__)

cli = cyclopts.App(name="research-module")


@cli.default()
def logging_setup() -> None:
    """Setup Logger for CLI."""
    logging.basicConfig(
        format="%(asctime)s %(name)s %(levelname)s: %(message)s",
        level=logging.INFO,
    )


cli.command(experiments_app)
cli.command(data_app)

if __name__ == "__main__":
    cli()
