"""Static paths of project."""

from pathlib import Path

ROOT_DIR = Path(__file__).parents[1]
DATA_DIR = ROOT_DIR / "data"
DATA_SOURCES = DATA_DIR / "datasources"
CONFIG_DIR = ROOT_DIR / "config"
RUNS_LOG_DIR = ROOT_DIR / "runs"
