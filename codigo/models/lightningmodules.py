"""Module detailing Lightning Modules."""

from collections.abc import Iterator
from typing import TypeAlias

import lightning.pytorch as pl
import torch.optim
from einops import rearrange
from omegaconf import DictConfig
from pytorch_lightning.utilities.types import OptimizerLRSchedulerConfig
from torchmetrics import MeanMetric, MetricCollection

from codigo.configs import Config
from codigo.data.utils import BatchT, CompBatchT, CutBatchT
from codigo.models.utils import ModeMetricDict
from codigo.utils import stage_to_string

PredT: TypeAlias = Iterator[
    tuple[str, torch.Tensor, torch.Tensor, torch.Tensor, dict[str, float]]
]


class Resnet50(pl.LightningModule):
    """Lightning Module for Resnet50.

    Args:
        config: Config class instance.
        metrics: Dictionary of metric name and torcheval Metric instance
    """

    def __init__(
        self, config: Config, metrics: MetricCollection, hparams: DictConfig | None
    ):
        super().__init__()
        self.config = config
        self.metrics_dict = ModeMetricDict(metrics)
        self.losses_dict = ModeMetricDict(
            MetricCollection({"loss": MeanMetric().to(config.device)})
        )
        self.model = config.model.instantiate()
        if config.model.freeze_encoder:
            self.model.encoder.requires_grad_(False)
        self.loss_fn = config.loss_fn.instantiate()
        self.save_hyperparameters(hparams)

    def _pred(self, x: torch.Tensor) -> torch.Tensor:
        return self.model(x).squeeze(dim=-3)

    def _step(self, batch: BatchT) -> float:
        x, target = batch[:2]
        pred = self._pred(x)
        stage = stage_to_string(self.trainer.state.stage)
        self.metrics_dict[stage].update(torch.nn.functional.sigmoid(pred), target)
        loss = self.loss_fn(pred, target)
        self.losses_dict[stage].update(loss)
        return loss

    def _log(self):
        stage = stage_to_string(self.trainer.state.stage)
        metrics = self.metrics_dict[stage].compute()
        self.metrics_dict[stage].reset()
        metrics |= self.losses_dict[stage].compute()
        self.losses_dict[stage].reset()
        self.log_dict(metrics, logger=True)

    CompleteImageStepReturnT = tuple[
        list[torch.Tensor], list[torch.Tensor], list[torch.Tensor], list[str]
    ]

    def training_step(  # type:ignore[override] # noqa: D102
        self, batch: CutBatchT, batch_idx: int
    ) -> float:
        return self._step(batch)

    def validation_step(self, batch: CompBatchT, batch_idx: int) -> None:  # noqa: D102
        self._step(batch)

    def test_step(self, batch: CompBatchT, batch_idx: int) -> None:  # noqa: D102
        self._step(batch)

    def predict_step(  # type:ignore[override] # noqa: D102
        self, batch: CompBatchT, batch_idx: int
    ) -> PredT:
        xs, targets, scene_names, patches_per_dim = batch
        preds = self._pred(xs)
        sigmoid_preds = torch.nn.functional.sigmoid(preds)

        acc_batch_patches = 0
        metric_list = []
        x_list = []
        target_list = []
        pred_list = []
        for h1, w1 in patches_per_dim:
            batch_patches = h1 * w1

            target = targets[acc_batch_patches:batch_patches]
            target_list.append(
                rearrange(
                    target, "(h1 w1) h2 w2 -> (h1 h2) (w1 w2)", h1=h1, w1=w1
                ).cpu()
            )

            sigmoid_pred = sigmoid_preds[acc_batch_patches:batch_patches]
            pred_list.append(
                rearrange(
                    sigmoid_pred, "(h1 w1) h2 w2 -> (h1 h2) (w1 w2)", h1=h1, w1=w1
                ).cpu()
            )

            metrics = {
                metric_name: metric(sigmoid_pred, target).item()
                for metric_name, metric in self.metrics_dict.metrics.items()
            }

            pred = preds[acc_batch_patches:batch_patches]

            metrics.update({"loss": self.loss_fn(pred, target).item()})

            metric_list.append(metrics)

            x_list.append(
                rearrange(
                    xs[acc_batch_patches:batch_patches],
                    "(h1 w1) c h2 w2 -> c (h1 h2) (w1 w2)",
                    h1=h1,
                    w1=w1,
                ).cpu()
            )
            acc_batch_patches += batch_patches

        return zip(scene_names, x_list, pred_list, target_list, metric_list)

    def on_train_epoch_end(self) -> None:  # noqa: D102
        self._log()

    def on_validation_epoch_end(self) -> None:  # noqa: D102
        self._log()

    def on_test_epoch_end(self) -> None:  # noqa: D102
        self._log()

    def configure_optimizers(self) -> OptimizerLRSchedulerConfig:  # noqa: D102
        optimizer = self.config.optimizer.instantiate(params=self.parameters())
        config: OptimizerLRSchedulerConfig = {"optimizer": optimizer}
        if (lr_scheduler := self.config.lr_scheduler) is not None:
            config.update(
                {"lr_scheduler": lr_scheduler.instantiate(optimizer=optimizer)}
            )
        return config
