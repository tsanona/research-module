"""Utilities for model related code."""

import numpy as np
import torch
import torch.nn as nn
from pytorch_lightning.trainer.trainer import RunningStage
from segmentation_models_pytorch.encoders.resnet import ResNetEncoder
from torchmetrics import MetricCollection


class SatelliteResnet50Encoder(ResNetEncoder):
    """Encoder built from ResNet50 model and satellite data pretrained.yaml weights.

    Pytorch resnet50 modified to accommodate satellite pretrained
        weights from https://github.com/HSG-AIML/SSLTransformerRS.
        It used weight from model pretrained.yaml with Sentinel-2 data.
    Instantiates an Encoder that can be used by the Unet model
        from Segmentation Models Pytorch library
        (https://github.com/qubvel/segmentation_models.pytorch).
    Currently, it accepts all 13 bands (or less) of Sentinel-2 data.
        If less bands are passed the first convolution's filters are
        restricted for the bands in use.

    """

    AVAILABLE_CHANNELS = (
        "B1",
        "B2",
        "B3",
        "B4",
        "B5",
        "B6",
        "B7",
        "B8",
        "B8A",
        "B9",
        "B10",
        "B11",
        "B12",
    )

    def __init__(self, bands: tuple[str, ...], out_channels: tuple[int, ...], **kwargs):
        out_channels = (len(self.AVAILABLE_CHANNELS), *out_channels[1:])
        super().__init__(out_channels, **kwargs)
        self._in_channels = self._out_channels[0]  # type:ignore[has-type]
        if self.conv1 != self._in_channels:
            self.conv1.weight = nn.parameter.Parameter(
                torch.Tensor(
                    self.conv1.out_channels,
                    self._in_channels // self.conv1.groups,
                    *self.conv1.kernel_size,
                )
            )
            self.conv1.reset_parameters()
        self._in_indexes = tuple(
            self.AVAILABLE_CHANNELS.index(band_name) for band_name in bands
        )

    def load_state_dict(  # noqa: D102
        self, state_dict: dict[str, np.ndarray], **kwargs
    ) -> None:
        state_dict.pop("fc.0.bias", None)
        state_dict.pop("fc.0.weight", None)
        state_dict.pop("fc.2.bias", None)
        state_dict.pop("fc.2.weight", None)
        super().load_state_dict(state_dict, **kwargs)

    def set_in_channels(self, in_channels: int, pretrained: bool):  # noqa: D102
        if in_channels != self._in_channels:
            # same code as segmentation_models_pytorch.encoders._utils.patch_first_conv
            weight = self.conv1.weight.detach()
            self.conv1.in_channels = in_channels

            if not pretrained:
                self.conv1.weight = nn.parameter.Parameter(
                    torch.Tensor(
                        self.conv1.out_channels,
                        in_channels // self.conv1.groups,
                        *self.conv1.kernel_size,
                    )
                )
                self.conv1.reset_parameters()

            elif in_channels == 1:
                new_weight = weight.sum(1, keepdim=True)
                self.conv1.weight = nn.parameter.Parameter(new_weight)

            else:
                # slight change, only use pretrained weights
                # for bands they are available for
                # otherwise just keep random initialization
                new_weight = torch.Tensor(
                    self.conv1.out_channels,
                    in_channels // self.conv1.groups,
                    *self.conv1.kernel_size,
                )
                new_weight[:, : len(self._in_indexes)] = weight[:, self._in_indexes]
                new_weight = new_weight * (self._in_channels / in_channels)
                self.conv1.weight = nn.parameter.Parameter(new_weight)

            self._in_channels = in_channels
            self._out_channels = (
                in_channels,
                *self._out_channels[1:],  # type:ignore[has-type]
            )


class ModeMetricDict(dict):
    """Dictionary that contains metrics for each mode of training."""

    def __init__(self, metrics: MetricCollection):
        super().__init__()
        self.metrics = metrics

    def __missing__(self, key: RunningStage):
        match self.metrics:
            case MetricCollection():
                new_item = self.metrics.clone(postfix=f"/{key}")
            case _:
                raise NotImplementedError
        self[key] = new_item
        return self[key]
