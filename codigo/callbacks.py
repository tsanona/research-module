"""Custom Callback functions for lightning."""

import lightning.pytorch as pl
import optuna as op
from omegaconf import DictConfig

from codigo.configs import Config
from codigo.utils import get_best_logged_metric, stage_to_string


class OptunaPruningCallback(  # type:ignore[misc]
    op.integration.PyTorchLightningPruningCallback, pl.Callback
):
    """Class wrapper for optuna integration with Pytorch Lightning."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class LogHyperparamBestMetricCallback(pl.Callback):
    """Callback to log hyper params and related best metric.

    Args:
        config: Config Class.
    """

    def __init__(self, config: Config, hparams: DictConfig):
        super().__init__()
        self.config = config
        self.hparams = hparams

    def _log_hyper_best_metric(self, trainer: pl.Trainer):
        assert (
            trainer.logger is not None and trainer.logger.log_dir is not None
        ), "Cannot access best metric"

        stage = stage_to_string(trainer.state.stage)

        best_metric = get_best_logged_metric(
            trainer.logger.log_dir,
            f"{self.config.hypertune.objective_metric}/{stage}",
        )
        trainer.logger.log_hyperparams(
            params=dict(self.hparams),
            metrics={
                f"best_{self.config.hypertune.objective_metric}/{stage}": best_metric
            },
        )

    def on_validation_end(  # noqa: D102
        self, trainer: "pl.Trainer", pl_module: "pl.LightningModule"
    ) -> None:
        self._log_hyper_best_metric(trainer)

    def on_test_end(  # noqa: D102
        self, trainer: "pl.Trainer", pl_module: "pl.LightningModule"
    ) -> None:
        self._log_hyper_best_metric(trainer)
