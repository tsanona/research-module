"""Utility functions for project."""

import operator
import os
import random
from collections.abc import Iterable
from itertools import filterfalse, tee
from pathlib import Path
from typing import Callable, Literal, TypeVar

import numpy as np
import torch
from lightning.pytorch.trainer.states import RunningStage
from omegaconf import DictConfig
from tbparse import SummaryReader
from tensorboard.backend.event_processing import event_accumulator

from codigo.paths import CONFIG_DIR

T = TypeVar("T")


def set_seed(seed: int) -> None:
    """Set random seed across python, torch and numpy.

    Args:
        seed: int to use as seed.
    """
    random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False


def parse_hyperparameters(dict_config: DictConfig) -> DictConfig | None:
    """Parse Hyperparameters in DictConfig object.

    Hyperparameters are defined as Dictionaries that contain the
        item "_target" with value "codigo.configs.HyperParameterChoice".

    Args:
        dict_config: Dictionary possibly containing hyperparameters.

    Returns:
        Dict config with found hyperparameters in same structure
            as they were found in. None if no hyperparameters are found.
    """
    hparams = DictConfig({})
    for item_name, value in dict_config.items():
        if isinstance(value, DictConfig):
            if value.get("_target_") == "codigo.configs.HyperParameterChoice":
                hparams[item_name] = value
            elif (parsed_dict := parse_hyperparameters(value)) is not None:
                hparams[item_name] = parsed_dict
    if hparams.is_empty():
        return None
    return hparams


def get_best_logged_metric(log_dir: str, metric: str) -> float:
    """Get best logged metric from logger files.

    Args:
        log_dir: Directory where logger files are.
        metric: Metric name to find best value of.

    Returns:
        Best logged metric value.
    """
    ea = event_accumulator.EventAccumulator(log_dir)
    ea.Reload()
    return max(
        ea.Scalars(metric),
        key=operator.attrgetter("value"),
    ).value


def append_search_path(overrides: list[str] | None) -> list[str]:
    """Add search path to overrides.

    Args:
        overrides: List of overrides or None

    Returns:
        List containing search path and other overrides if they were not None.
    """
    match overrides:
        case list():
            return overrides + [f"hydra.searchpath=[file://{CONFIG_DIR}/commons]"]
        case None:
            return [f"hydra.searchpath=[file://{CONFIG_DIR}/commons]"]


def stage_to_string(stage: RunningStage | None) -> str:
    """Transform stage into string.

    Args:
        stage: Trainer RunningSatge.

    Returns:
        String version of stage.

    Raises:
        RuntimeError: If stage or dataloader_prefix are None
    """
    if stage is not None:
        str_stage = stage.dataloader_prefix
        if str_stage is not None:
            return str_stage
    raise RuntimeError("Trainer stage could not be determined")


def return_if_unique(prev: T, following: T | None) -> T:
    """Return prev if the is no following.

    Args:
        prev: Element to be returned.
        following: control element.

    Returns:
        Value of prev

    Raises:
        ValueError: If  following is not None.
    """
    if following is None:
        return prev
    raise ValueError("Following is not None!")


def is_of_phase(phase: Literal["train", "test"], tfevent: Path) -> bool:
    """Check if tfevent contains metrics from phase.

    Args:
        phase: Either train or test as literals.
        tfevent: Path to tensorboard log file to be checked.

    Returns:
        True if tfevent file contains metrics from given phase, False otherwise.
    """
    return any(
        map(lambda tag: phase in tag, SummaryReader(str(tfevent)).tags["scalars"])
    )


def partition(
    pred: Callable[[T], object] | None | None, iterable: Iterable[T]
) -> tuple[list[T], list[T]]:
    """Partition entries into false entries and true entries.

    If *pred* is slow, consider wrapping it with functools.lru_cache().
    From https://docs.python.org/dev/library/itertools.html#itertools-recipes
    Added types and list returns.

    Args:
        pred: Predicate function that takes iterable item and return bool.
        iterable: Iterable to be partitioned.

    Returns:
        Two lists of negative items and positive items respectively.
    """
    # partition(is_odd, range(10)) --> 0 2 4 6 8   and  1 3 5 7 9
    t1, t2 = tee(iterable)
    return list(filterfalse(pred, t1)), list(filter(pred, t2))
