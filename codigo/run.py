"""Functions to run complete experiment or its specific stages."""

import json
import logging
import os
import shutil
import tempfile
from collections import defaultdict
from collections.abc import Callable
from dataclasses import dataclass
from functools import reduce
from itertools import chain, repeat
from pathlib import Path
from typing import Literal, Optional, cast

import click
import cyclopts
import hydra
import multiprocess as mp
import numpy as np
import omegaconf
import optuna as op
from lightning import seed_everything
from omegaconf import DictConfig, OmegaConf
from typing_extensions import Annotated

from codigo.configs import Config
from codigo.data.datasets_datamodules import SceneDataModule
from codigo.models.lightningmodules import PredT
from codigo.module_setup import setup_data, setup_modules, setup_optuna
from codigo.paths import CONFIG_DIR, ROOT_DIR, RUNS_LOG_DIR
from codigo.utils import (
    append_search_path,
    get_best_logged_metric,
    is_of_phase,
    parse_hyperparameters,
    return_if_unique,
    set_seed,
)

logger = logging.getLogger(__name__)


def get_trial(
    config: Config,
    hparam_choices: DictConfig,
    run_path: Path,
    data_module: SceneDataModule,
) -> Callable[[op.Trial], float]:
    """Generate a trial running function.

    Args:
        config: Config class instance.
        hparam_choices: DictConfig mapping hparams to HyperParameterChoice instances.
        run_path: Path of the run setting up for.
        data_module: Pytorch lightning data module instance.

    Returns:
        Trial running function.
    """

    def run_trial(trial: op.Trial) -> float:
        """Execute Trial.

        Args:
            trial: Optuna trial instance

        Returns:
            Best metric value obtained from run.
        """
        set_seed(config.seed)
        trial_config, chosen_hparams = config.get_trial_config(
            hparam_choices=hparam_choices, trial=trial
        )

        seed_everything(config.seed, workers=True)
        trainer, model_module = setup_modules(
            trial_config, run_path, trial, chosen_hparams, False
        )
        with data_module.copy() as data_module_copy:
            trainer.fit(
                model=model_module,
                datamodule=data_module_copy,
            )
        assert (
            trainer.logger is not None and trainer.logger.log_dir is not None
        ), "Cannot access best metric"
        return get_best_logged_metric(
            trainer.logger.log_dir,
            f"{trial_config.hypertune.objective_metric}/val",
        )

    return run_trial


def run_hypertune(
    config: Config,
    hparam_choices: DictConfig,
    run_path: Path,
    data_module: SceneDataModule,
) -> op.Study:
    """Execute hyperparameter tuning.

    Args:
        config: Config class instance.
        hparam_choices: DictConfig mapping hparams to HyperParameterChoice instances.
        run_path: Path where running artifacts are stored.
        data_module: Pytorch lightning data module instance.

    Returns:
        Optuna study containing tuning runs.
    """
    run_path = run_path / "hypertuning"
    if not run_path.is_dir():
        run_path.mkdir(parents=True, exist_ok=True)
    study = setup_optuna(config, run_path)
    completed_trials = [
        trial for trial in study.trials if trial.state == op.trial.TrialState.COMPLETE
    ]
    if completed_trials and not click.confirm(
        f"Study already has {len(completed_trials)} trials."
        f"Would you like to run more trials?"
    ):
        logger.info("Hyperparameter run will be skipped")
        return study
    trial = get_trial(
        config=config,
        hparam_choices=hparam_choices,
        run_path=run_path,
        data_module=data_module,
    )
    if config.hypertune.num_workers < 2:
        study.optimize(
            trial,
            config.hypertune.trials_per_worker,
        )
    else:
        ctx = mp.context.SpawnContext()
        with ctx.Pool(
            processes=config.hypertune.num_workers, initializer=study.sampler.reseed_rng
        ) as pool:
            for trial in repeat(trial, config.hypertune.total_num_trials):
                pool.apply_async(
                    study.optimize,
                    args=(trial,),
                    kwds={
                        "n_trials": 1,
                        "gc_after_trial": True,
                    },
                )
            pool.close()
            pool.join()
    return study


experiments_app = cyclopts.App(name="experiment")


@experiments_app.command()
def run_full(
    experiment_name: Annotated[str, cyclopts.Parameter()],
    model_type: Annotated[str, cyclopts.Parameter()],
    /,
    run_dir: Annotated[Path, cyclopts.Parameter()] = RUNS_LOG_DIR,
    overrides: Annotated[Optional[list[str]], cyclopts.Parameter()] = None,
) -> None:
    """Run hyperparameter tuning, training/val and testing.

    Args:
        experiment_name: Name experiment.
        model_type: Name of experiment to be trained.
        run_dir: Directory for training files.
        overrides: Overrides config.
    """
    with hydra.initialize_config_dir(
        config_dir=str(CONFIG_DIR / "experiments" / model_type), version_base=None
    ):
        conf = hydra.compose(
            config_name="config", overrides=append_search_path(overrides)
        )

    if (hparams_choices := parse_hyperparameters(conf)) is not None:
        hparams_choices = hydra.utils.instantiate(hparams_choices)

    config: Config = hydra.utils.instantiate(conf)

    pretrained = (
        config.model.params is not None
        and (pretraining_data := config.model.params.get("encoder_weights")) is not None
    )

    if not run_dir.is_absolute():
        run_dir = ROOT_DIR / run_dir
    experiment_run_path = run_dir / experiment_name
    if not experiment_run_path.is_dir():
        experiment_run_path.mkdir(parents=True, exist_ok=True)

    seed_everything(config.seed, workers=True)
    data_module = setup_data(config, experiment_run_path)

    ablation_name = (
        f"pretrained-{pretraining_data}-{model_type}" if pretrained else f"{model_type}"
    )
    if config.model.freeze_encoder:
        ablation_name = "frozen-" + ablation_name
    ablation_run_path = experiment_run_path / ablation_name
    if not ablation_run_path.is_dir():
        ablation_run_path.mkdir(parents=True, exist_ok=True)

    omegaconf.OmegaConf.save(conf, ablation_run_path / "config.yaml")

    while (fold := data_module.fold_step()) is not None:
        fold_run_path = ablation_run_path / f"fold_{fold}"
        if not fold_run_path.is_dir():
            fold_run_path.mkdir(parents=True, exist_ok=True)

        chosen_hparams = None
        if hparams_choices is not None:
            study = run_hypertune(config, hparams_choices, fold_run_path, data_module)
            config, chosen_hparams = config.get_tuned_config(
                hparams_choices, study.best_params
            )

        seed_everything(config.seed, workers=True)

        trainer, model_module = setup_modules(
            config, fold_run_path, None, chosen_hparams, False
        )
        with data_module.copy() as data_module_copy:
            trainer.fit(
                model=model_module,
                datamodule=data_module_copy,
            )
            trainer.test(
                model=model_module,
                datamodule=data_module_copy,
                ckpt_path="best",
            )


@experiments_app.command()
def rerun(
    phase: Annotated[Literal["train", "test"], cyclopts.Parameter()],
    /,
    run_dir: Annotated[Path, cyclopts.Parameter()] = RUNS_LOG_DIR,
    overrides: Annotated[Optional[list[str]], cyclopts.Parameter()] = None,
) -> None:
    """Rerun training and/or testing for experiments in run dir.

    Finds all experiments, their ablations and folds and
    reruns training and/or testing using hyperparameters
    from already run hyperparameter tunings.

    Args:
        phase: Phase to rerun.
        run_dir: Dir where all experiments are.
        overrides: Overrides config.

    Raises:
        RuntimeError: If there are more than one ckpts available.
    """
    if not run_dir.is_absolute():
        run_dir = ROOT_DIR / run_dir
    for config_file_path in run_dir.rglob("*config.yaml"):
        ablation_path = config_file_path.parent
        with hydra.initialize_config_dir(
            config_dir=str(ablation_path), version_base=None
        ):
            conf = hydra.compose(config_name="config", overrides=overrides)
        config: Config = hydra.utils.instantiate(conf)
        data_module = setup_data(config, None)
        while (fold := data_module.fold_step()) is not None:
            training_dir = ablation_path / f"fold_{fold}" / "training"
            hparams = cast(DictConfig, OmegaConf.load(training_dir / "hparams.yaml"))
            config = hydra.utils.instantiate(OmegaConf.merge(conf, hparams))
            with tempfile.TemporaryDirectory() as tmpdirname:
                temp_dir = Path(tmpdirname)
                trainer, model_module = setup_modules(
                    config, temp_dir, None, hparams, False
                )
                try:
                    ckpt_path: Path | str = reduce(
                        return_if_unique, training_dir.glob("training*.ckpt")
                    )
                except ValueError as e:
                    raise RuntimeError(
                        f"There are multiple ckpt for {training_dir}"
                    ) from e
                if phase == "train":
                    trainer.fit(
                        model=model_module,
                        datamodule=data_module,
                    )
                    ckpt_path = "best"
                trainer.test(
                    model=model_module,
                    datamodule=data_module,
                    ckpt_path=ckpt_path,
                )
                for item in training_dir.iterdir():
                    if phase == "test" and (
                        item.suffix.endswith("ckpt")
                        or item.suffix.endswith("yaml")
                        or ("tfevents" in item.name and not is_of_phase(phase, item))
                    ):
                        continue
                    os.remove(item)
                for item in (temp_dir / "training").iterdir():
                    shutil.move(item, training_dir / item.name)


@dataclass
class BestModelMetadata:
    """Holds Best model metadata.

    Args:
        ckpt_path: Path to best model.
        metric_value: Objective_metric value of best model.
        hparams: Hyper parameters used for best model.
    """

    ckpt_path: Path
    metric_value: float
    hparams: DictConfig


@experiments_app.command()
def run_predict(
    run_dir: Annotated[Path, cyclopts.Parameter()] = RUNS_LOG_DIR,
    /,
    overrides: Annotated[Optional[list[str]], cyclopts.Parameter()] = None,
) -> None:
    """Run prediction for each ablation of experiment.

    Args:
        run_dir: Dir where all experiments are.
        overrides: Overrides config.
    """
    if not run_dir.is_absolute():
        run_dir = ROOT_DIR / run_dir
    for config_file_path in run_dir.rglob("*config.yaml"):
        ablation_path = config_file_path.parent
        with hydra.initialize_config_dir(
            config_dir=str(ablation_path), version_base=None
        ):
            conf = hydra.compose(config_name="config", overrides=overrides)
        config = hydra.utils.instantiate(conf)

        def _fold_path_to_best_model_metadata(fold_path: Path) -> BestModelMetadata:
            training_dir = fold_path / "training"
            metric_value = get_best_logged_metric(
                str(training_dir), f"{config.hypertune.objective_metric}/test"
            )
            try:
                ckpt_path = reduce(
                    return_if_unique, training_dir.glob("training*.ckpt")
                )
            except ValueError as e:
                raise RuntimeError(f"There are multiple ckpt for {training_dir}") from e
            return BestModelMetadata(
                ckpt_path=ckpt_path,
                metric_value=metric_value,
                hparams=cast(
                    DictConfig, OmegaConf.load(str(training_dir / "hparams.yaml"))
                ),
            )

        # find best performing cross validation fold to run prediction on.
        best_fold_model_metadata = reduce(
            lambda prev, current: (
                prev if prev.metric_value > current.metric_value else current
            ),
            map(_fold_path_to_best_model_metadata, ablation_path.glob("fold_*")),
        )

        run_path = best_fold_model_metadata.ckpt_path.parents[2]
        config = hydra.utils.instantiate(
            OmegaConf.merge(conf, best_fold_model_metadata.hparams)
        )

        data_module = setup_data(config, None)
        trainer, model_module = setup_modules(
            config, run_path, None, best_fold_model_metadata.hparams, True
        )

        batched_predictions = cast(
            list[PredT],
            trainer.predict(
                model=model_module,
                datamodule=data_module,
                ckpt_path=str(best_fold_model_metadata.ckpt_path),
            ),
        )

        predictions_path = ablation_path / "predictions"
        predictions = chain.from_iterable(batched_predictions)
        if not predictions_path.is_dir():
            predictions_path.mkdir(parents=True, exist_ok=True)
        OmegaConf.save(
            best_fold_model_metadata.hparams, predictions_path / "hparams.yaml"
        )
        image_metrics_dict: dict[str, dict[str, float]] = defaultdict()
        for scene_name, original, predicted, target, metrics in predictions:
            image_idx_path = predictions_path / str(scene_name)
            if not image_idx_path.is_dir():
                image_idx_path.mkdir(parents=True, exist_ok=True)
            image_metrics_dict[scene_name] = metrics
            np.save(str(image_idx_path / "original"), original)
            np.save(str(image_idx_path / "predicted"), predicted)
            np.save(str(image_idx_path / "target"), target)
        with (predictions_path / "image_metrics_dict.json").open("w") as fp:
            json.dump(image_metrics_dict, fp)
