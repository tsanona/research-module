"""Costume loss functions."""

import math
from typing import Literal

import torch
from beartype import beartype
from pydantic import PositiveFloat


class LogCoshLoss(torch.nn.Module):
    """Implementation of LogCosh loss.

    LogCosh loss from https://arxiv.org/pdf/2006.14822.pdf
    Code from https://datascience.stackexchange.com/questions/96271/logcoshloss-on-pytorch

    Args:
        aggregation: Means of agregation of elementwhise losses.
        Possible values are: mean, sum or Non to return elementwhise loss.
    """

    def __init__(self, aggregation: Literal["mean", "sum"] | None):
        super().__init__()
        self.aggregation = aggregation

    @staticmethod
    def _loss_function(preds: torch.Tensor, targets: torch.Tensor) -> torch.Tensor:
        error = torch.sigmoid(preds) - targets
        return error + torch.nn.functional.softplus(-2.0 * error) - math.log(2.0)

    def forward(  # noqa: D102
        self, preds: torch.Tensor, targets: torch.Tensor
    ) -> torch.Tensor:
        loss = self._loss_function(preds, targets)
        if self.aggregation is not None:
            loss = getattr(torch, self.aggregation)(loss)
        return loss


class FocalTverskyLoss(torch.nn.Module):
    """Implementation of Focal Tversky loss.

    https://arxiv.org/pdf/1810.07842.pdf

    Args:
        alpha:
        beta:
        gamma:
        aggregation:
    """

    @beartype
    def __init__(
        self,
        alpha: PositiveFloat,
        beta: PositiveFloat | None,
        gamma: PositiveFloat,
        aggregation: Literal["mean", "sum"] | None,
    ):
        super().__init__()
        self.gamma = gamma
        self.aggregation = aggregation
        self.eps = 1e-7
        if beta is None:
            beta = 1 - alpha
        self.register_buffer(
            "kernel", torch.tensor([[[1, alpha, beta]]]), persistent=False
        )

    def _loss_function(
        self, preds: torch.Tensor, targets: torch.Tensor
    ) -> torch.Tensor:
        # ((B) H W) -> (1 (B) H W)
        pred_prob = torch.sigmoid(preds).unsqueeze(0)
        targets = targets.unsqueeze(0)

        tp_fn_fp_tn = (
            torch.multiply(
                # (4 (B) H W)
                torch.vstack([targets, targets, 1 - targets, 1 - targets]),
                torch.vstack([pred_prob, 1 - pred_prob, pred_prob, 1 - pred_prob]),
            )
            # (4 (B))
            .sum(dim=[-1, -2])
            # ((B) 4)
            .T
        )
        # ((B) 2)
        bases = torch.clip(
            # ((B) 1 2)
            torch.nn.functional.conv1d(
                # ((B) 1 6)
                torch.nn.functional.pad(tp_fn_fp_tn.unsqueeze(-2), (0, 2), "reflect"),
                # (1 1 3)
                self.kernel.to(device=tp_fn_fp_tn.device),
                stride=(3,),
            ),
            min=self.eps,
        ).squeeze(-2)
        # ((B))
        return torch.sum(
            (
                1
                - torch.div(
                    torch.index_select(
                        tp_fn_fp_tn, -1, torch.tensor([0, 3], device=tp_fn_fp_tn.device)
                    ),
                    bases,
                )
            )
            ** 1
            / self.gamma,
            dim=-1,
        )

    def forward(  # noqa: D102
        self, preds: torch.Tensor, targets: torch.Tensor
    ) -> torch.Tensor:
        loss = self._loss_function(preds, targets)
        if self.aggregation is not None:
            loss = getattr(torch, self.aggregation)(loss)
        return loss
