[Wu et al.](https://www.mdpi.com/2072-4292/12/24/4020)

- u-net with Tversky loss (because of unbalance between lake vs no lake)
- October to November in 2015 Landsat 8 (this timing because of lack of clouds)
- S1 asc + desc (summed and cut of at a threshold of 1) -> maybe log-normalize and add both types as channels and not bother to process?
- Ground truth are contours

[Qayyum et al.](https://www.mdpi.com/2220-9964/9/10/560/htm)

- EfficientNet with weighted loss (because of unbalance between lake vs no lake)
- Planet scope
- Ground truth are rasterized poligons
