from setuptools import find_packages, setup

setup(
    name="research-module",
    version="0.1",
    description="Detection of glacial lakes",
    author="Tiago Sanona",
    author_email="desousasanona@uni-potsdam.de",
    license="",
    packages=find_packages(),
    entry_points={
        "console_scripts": ["research-module=codigo.cli:cli"]
    },
)
