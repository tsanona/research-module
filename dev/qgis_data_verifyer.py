import ee
from ee_plugin import Map

dataset = ee.ImageCollection("COPERNICUS/S2").filterBounds(Map.getCenter()).filterDate('2017-09-10', '2017-09-30');
datasetList = dataset.toList(dataset.size())
img = ee.Image(datasetList.get(0))

Map.addLayer(img, {'min':500, 'max':5000, 'gamma':1.4, 'bands': ['B4', 'B3', 'B2']}, 'image')