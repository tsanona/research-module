.PHONY: help requirements install install-ci format lint test coverage


SHELL := /bin/bash
PLATFORM := $(shell \
  if [[ -n $$(command -v nvidia-smi && nvidia-smi --list-gpus) ]]; then echo gpu; \
  elif [[ $$(uname -s) == Darwin ]]; then echo osx; \
  else echo cpu; \
  fi)

help:
	@echo "Available commands:"
	@echo "requirements       compile all requirements."
	@echo "install            install dev requirements for CPU only."
	@echo "format             format code."
	@echo "lint               run linters."
	@echo "test               run unit tests."
	@echo "coverage           build coverage report."

requirements:
	pip install -q --upgrade pip-compile-multi

ifeq (${PLATFORM}, cpu)
	pip-compile-multi \
	  -t requirements/requirements.in \
	  -t requirements/requirements-ci.in \
	  -t requirements/requirements-dev.in \
	  --extra-index-url https://download.pytorch.org/whl/cpu \
	  --backtracking
endif
ifeq (${PLATFORM}, gpu)
	pip-compile-multi \
	  -t requirements/requirements.in \
	  -t requirements/requirements-gpu.in \
	  -t requirements/requirements-ci.in \
	  -t requirements/requirements-dev.in \
	  --backtracking
endif
ifeq (${PLATFORM}, osx)
	pip-compile-multi \
	  -t requirements/requirements.in \
	  -t requirements/requirements-ci.in \
	  -t requirements/requirements-dev.in \
	  --backtracking
endif



.venv_timestamp: requirements/*.txt
	pip install -q --upgrade pip-tools

ifeq (${PLATFORM}, gpu)
	pip-sync -q \
	  requirements/requirements.txt \
	  requirements/requirements-dev.txt \
	  requirements/requirements-ci.txt \
	  requirements/requirements-gpu.txt
else
	pip-sync -q \
	  requirements/requirements.txt \
	  requirements/requirements-dev.txt \
	  requirements/requirements-ci.txt
endif

	pip install -q -e .
	touch .venv_timestamp

install: .venv_timestamp

format: install
	isort codigo
	black codigo

mypy: install
	mypy codigo

linter: install
	flake8 codigo

lint: mypy linter

test: install
	pytest -v

integration-test: install
	pytest -v -m integration

coverage: install
	coverage run --source main -m pytest -v
	coverage report
	coverage xml
