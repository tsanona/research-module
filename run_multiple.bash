#!/bin/bash

export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

pyenv activate research-module

experiement_name='full_data'
models='rgb multispectrum'
pretrained_opts='model=sen12ms model=default'
overides="dataset.keep_ratio=1"

for model in $models
do
for pretrained in $pretrained_opts
do
research-module experiment run-full "$experiement_name" "$model" "$pretrained" $overides
if [ "$pretrained" = 'model=sen12ms' ]; then
research-module experiment run-full "$experiement_name" "$model" "$pretrained" 'model.freeze_encoder=True' $overides
fi
done
done

encoder_modes='model.freeze_encoder=True model.freeze_encoder=False'

for encoder_mode in $encoder_modes
do
research-module experiment run-full "$experiement_name" rgb "model=imagenet" "$encoder_mode" $overides
done